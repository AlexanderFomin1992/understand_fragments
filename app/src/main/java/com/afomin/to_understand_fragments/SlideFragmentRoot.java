package com.afomin.to_understand_fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class SlideFragmentRoot extends Fragment {

    public SlideFragmentRoot() {
        // Required empty public constructor
    }

    ViewPager pager;
    PagerAdapter pagerAdapter;
    View view;

    public static SlideFragmentRoot newInstance() {
        SlideFragmentRoot fragment = new SlideFragmentRoot();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_slide_fragment_root, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        pager = view.findViewById(R.id.pager);
        pagerAdapter = new PagerAdapter(getChildFragmentManager());
        pager.setAdapter(pagerAdapter);
    }
}
