package com.afomin.to_understand_fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class DetailedItemFragment extends Fragment {

    TextView textViewID;
    String idToSet = "-1";

    public DetailedItemFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DetailedItemFragment newInstance(String idToSet) {
        DetailedItemFragment fragment = new DetailedItemFragment();
        fragment.idToSet = idToSet;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detailed_item, container, false);
        textViewID = view.findViewById(R.id.text_view_id);
        textViewID.setText(idToSet);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setID(String id) {
        textViewID.setText(id);
    }
}
