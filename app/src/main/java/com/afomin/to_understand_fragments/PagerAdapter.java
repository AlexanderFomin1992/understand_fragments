package com.afomin.to_understand_fragments;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {

    private final static int COUNT_OF_CHILDS = 2;
    private final static int SIBLINGS_POSITION = 0;
    private final static int DOUBLE_ACTIVITIES_POSITION = 1;

    public PagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case (SIBLINGS_POSITION): {
                fragment = SiblingsFragment.newInstance();
                break;
            }
            case (DOUBLE_ACTIVITIES_POSITION):{
                fragment = DoubleBrothersFragment.newInstance();
                break;
            } default: {
                return null;
            }
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return COUNT_OF_CHILDS;
    }
}
