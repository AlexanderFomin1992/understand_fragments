package com.afomin.to_understand_fragments;

import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "MainActivity";

    CompositeDisposable compositeDisposable = new CompositeDisposable();
    SharedPreferences sharedPreferences;
    FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences(getResources().getString(R.string.shared_preferences_file_name), MODE_PRIVATE);
        boolean showIntro = sharedPreferences.getBoolean(getResources().getString(R.string.shared_preferences_show_intro_fragment_key), true);
        fragmentManager = getSupportFragmentManager();
        if (showIntro && savedInstanceState == null) {
            IntroFragment introFragment = IntroFragment.newInstance();
            fragmentManager
                    .beginTransaction()
                    .add(R.id.fragments_layout,introFragment)
                    .commit();
            compositeDisposable.add(Completable
                    .complete()
                    .delay(3, TimeUnit.SECONDS)
                    .subscribe(this::expandBusinessLogicFragments));
        } else {
            expandBusinessLogicFragments();
        }
        sharedPreferences
                .edit()
                .putBoolean(getResources().getString(R.string.shared_preferences_show_intro_fragment_key), !showIntro)
                .apply();
    }

    private void expandBusinessLogicFragments() {
        SlideFragmentRoot slideFragmentRoot = SlideFragmentRoot.newInstance();
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragments_layout, slideFragmentRoot)
                .commit();
    }

    @Override
    public void onStop() {
        super.onStop();
        compositeDisposable.dispose();
    }
}
