package com.afomin.to_understand_fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DoubleBrothersFragment extends Fragment {

    BrothersViewModel brothersViewModel;

    public DoubleBrothersFragment() {
        // Required empty public constructor
    }

    public static DoubleBrothersFragment newInstance() {
        DoubleBrothersFragment fragment = new DoubleBrothersFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_double_brothers, container, false);
        brothersViewModel = ViewModelProviders.of(this).get(BrothersViewModel.class);
        BrothersChildFragment firstBrothersChildFragment = BrothersChildFragment.newInstance();
        BrothersChildFragment secondBrothersChildFragment = BrothersChildFragment.newInstance();

        getChildFragmentManager()
                .beginTransaction()
                .add(R.id.first_brother_frame_layout, firstBrothersChildFragment)
                .commit();
        getChildFragmentManager()
                .beginTransaction()
                .add(R.id.second_brother_frame_layout, secondBrothersChildFragment)
                .commit();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
