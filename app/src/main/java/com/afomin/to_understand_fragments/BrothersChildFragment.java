package com.afomin.to_understand_fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class BrothersChildFragment extends Fragment {
    private static final String TAG = "BrothersChildFragment";


    EditText editText;
    Button button;

    public BrothersChildFragment() {
        // Required empty public constructor
    }

    public static BrothersChildFragment newInstance() {
        BrothersChildFragment fragment = new BrothersChildFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_brothers_child, container, false);
        editText = view.findViewById(R.id.edit_text);
        button = view.findViewById(R.id.button);


        // Create the observer which updates the UI.
        final Observer<String> storedValueObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String newStoredValue) {
                Log.d(TAG, "onChanged: " + newStoredValue);
                editText.setText(newStoredValue);
            }
        };
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        ((DoubleBrothersFragment)getParentFragment()).brothersViewModel.getStoredValue().observe(
                this, storedValueObserver);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick");
                ((DoubleBrothersFragment)getParentFragment()).brothersViewModel.getStoredValue().setValue(
                        editText.getText().toString());
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
