package com.afomin.to_understand_fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afomin.to_understand_fragments.dummy.DummyContent;


public class SiblingsFragment extends Fragment implements ItemsListFragment.OnListFragmentInteractionListener {

    public final static String TAG = "SiblingsFragment";
    private boolean isTwoPanel;
    DetailedItemFragment detailedItemFragment;

    FragmentManager fragmentManager;
    public SiblingsFragment() {
        // Required empty public constructor
    }

    public static SiblingsFragment newInstance() {
        SiblingsFragment fragment = new SiblingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // add some dummy content to list
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_siblings, container, false);
        isTwoPanel = view.findViewById(R.id.frame_layout_item) != null;
        Log.d(TAG, "onCreateView orientation: " + getResources().getConfiguration().orientation);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        fragmentManager = getChildFragmentManager();
        ItemsListFragment itemsListFragment = ItemsListFragment.newInstance();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_layout_list, itemsListFragment)
                .commit();
        if (isTwoPanel) {
            DetailedItemFragment detailedItemFragment = new DetailedItemFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_layout_item, detailedItemFragment)
                    .commit();
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {
        int frameId = isTwoPanel? R.id.frame_layout_item : R.id.frame_layout_list;
        DetailedItemFragment detailedItemFragment = DetailedItemFragment.newInstance(item.id);
        fragmentManager.beginTransaction()
                .replace(frameId, detailedItemFragment)
                .addToBackStack(null)
                .commit();
    }
}
