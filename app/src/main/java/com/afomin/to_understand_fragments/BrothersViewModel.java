package com.afomin.to_understand_fragments;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BrothersViewModel extends ViewModel {

    private MutableLiveData<String> storedValue;

    public MutableLiveData<String> getStoredValue() {
        if (storedValue == null) {
            storedValue = new MutableLiveData<String>();
        }
        return storedValue;
    }
}
